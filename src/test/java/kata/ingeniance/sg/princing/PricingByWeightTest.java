package test.java.kata.ingeniance.sg.princing;

import main.java.kata.ingeniance.sg.model.ProductOffer;
import main.java.kata.ingeniance.sg.model.Unit;
import main.java.kata.ingeniance.sg.pricing.PricingByWeight;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


public class PricingByWeightTest {

    private PricingByWeight pricingByWeight;

    @Test
    public void testCalculateThePriceOfProductsByWeight() {
        //given
        ProductOffer productA = new ProductOffer(4.4f,10, "item5", 22f);
        productA.setUnit(Unit.POUND.getValue());
        ProductOffer productB = new ProductOffer(4.4f,10, "item6", 22f);
        productB.setUnit(Unit.OUNCES.getValue());
        ProductOffer productC = new ProductOffer(4.4f,10, "item7", 22f);
        productC.setUnit(3);
        ProductOffer productD = new ProductOffer(0.0f,10, "item6", 22f);
        productB.setUnit(Unit.OUNCES.getValue());
        pricingByWeight = new PricingByWeight();

        //then
        assertThat(pricingByWeight.calculatePrice(productA), is(96.8f));
        assertThat(pricingByWeight.calculatePrice(productB), is(6.05f));
        assertThat(pricingByWeight.calculatePrice(productC), is(0.0f));
        assertThat(pricingByWeight.calculatePrice(productD), is(0.0f));
    }
}
