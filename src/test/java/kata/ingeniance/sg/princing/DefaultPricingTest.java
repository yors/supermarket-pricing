package test.java.kata.ingeniance.sg.princing;

import main.java.kata.ingeniance.sg.model.ProductOffer;
import main.java.kata.ingeniance.sg.pricing.DefaultPricing;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class DefaultPricingTest {

    private DefaultPricing defaultPricing;

    @Test
    public void testCalculateTheDeFaultPriceOfProducts() {
        //given
        ProductOffer productA = new ProductOffer(null, 2, "item1", 4.5f);
        ProductOffer productB = new ProductOffer(null, 2, "item2", 0f);
        defaultPricing = new DefaultPricing();

        //then
        assertThat(defaultPricing.calculatePrice(productA), is(9f));
        assertThat(defaultPricing.calculatePrice(productB), is(0f));
    }
}
