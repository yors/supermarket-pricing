package test.java.kata.ingeniance.sg.princing;

import main.java.kata.ingeniance.sg.model.ProductOffer;
import main.java.kata.ingeniance.sg.pricing.PricingGetOneFree;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class PricingGetOneFreeTest {

    private PricingGetOneFree pricingGetOneFree;

    @Test
    public void testCalculateThePriceWithOneProductFree() {
        //given
        ProductOffer productA = new ProductOffer(0.0f,2, "item5", 22f);
        ProductOffer productB = new ProductOffer(0.0f,6, "item6", 22f);
        ProductOffer productC = new ProductOffer(0.0f,8, "item7", 22f);
        ProductOffer productD = new ProductOffer(0.0f,5, "item9", 22f);
        ProductOffer productE = new ProductOffer(0.0f,1, "item6", 22f);
        pricingGetOneFree = new PricingGetOneFree();

        //then
        assertThat(pricingGetOneFree.calculatePrice(productA), is(44f));
        assertThat(pricingGetOneFree.calculatePrice(productB), is(110f));
        assertThat(pricingGetOneFree.calculatePrice(productC), is(154f));
        assertThat(pricingGetOneFree.calculatePrice(productD), is(0.0f));
        assertThat(pricingGetOneFree.calculatePrice(productE), is(22f));
    }
}
