package test.java.kata.ingeniance.sg.princing;

import main.java.kata.ingeniance.sg.model.ProductOffer;
import main.java.kata.ingeniance.sg.pricing.PromotionPricing;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;


public class PromotionPricingTest {

    private PromotionPricing promotionPricing;

    @Test
    public void testCalculateThePromotionPriceOfProducts() {
        //given
        ProductOffer productA = new ProductOffer(0.0f,10, "item4", 22f);
        ProductOffer productB = new ProductOffer(0.0f,6, "item4", 22f);
        ProductOffer productC = new ProductOffer(0.0f,4, "item4", 22f);
        ProductOffer productD = new ProductOffer(0.0f,4, "item20", 22f);
        promotionPricing = new PromotionPricing();

        //then
        assertThat(promotionPricing.calculatePrice(productA), is(94f));
        assertThat(promotionPricing.calculatePrice(productB), is(6f));
        assertThat(promotionPricing.calculatePrice(productC), is(88f));
        assertThat(promotionPricing.calculatePrice(productD), is(0.0f));
    }
}
