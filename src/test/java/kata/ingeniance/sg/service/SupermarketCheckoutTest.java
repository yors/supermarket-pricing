package test.java.kata.ingeniance.sg.service;

import main.java.kata.ingeniance.sg.model.ProductOffer;
import main.java.kata.ingeniance.sg.model.Unit;
import main.java.kata.ingeniance.sg.service.ProductCart;
import main.java.kata.ingeniance.sg.service.SupermarketCheckout;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import java.util.HashMap;
import java.util.Map;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;

public class SupermarketCheckoutTest {

    private static SupermarketCheckout supermarket;
    private static ProductCart productCart;
    Map<String,ProductOffer > productListMap;

    @BeforeClass
    public static void initSupermarket() {
        supermarket = new SupermarketCheckout();

    }
    @Before
    public void init() {
        productListMap = new HashMap < String, ProductOffer > ();
        productCart = new ProductCart();
    }

    @After
    public void restoreStreams() {
        productListMap = null;
    }

    @Test
    public void processDefault_pricing_test() {

        //given
        Float result = 0.0f;
        productListMap.put("item5", new ProductOffer(null, 10, "item5", 22f));
        productListMap.put("item2", new ProductOffer(null, 6, "item2", 2f));

        result = supermarket.processDefaultPricing(productListMap, result);

        //then
        assertThat(result, is(232f));
    }

    @Test
    public void process_pricing_by_weight_test() {

        //given
        Float result = 0.0f;
        ProductOffer productA = new ProductOffer(4.4f, 10, "item5", 22f);
        productA.setUnit(Unit.POUND.getValue());
        ProductOffer productB = new ProductOffer(2.4f, 10, "item6", 22f);
        productB.setUnit(Unit.OUNCES.getValue());
        productListMap.put("item5", productA);
        productListMap.put("item6", productB);

        result = supermarket.processPricingByWeight(productListMap, result);
        result = ((float)((int)(result * 100))) / 100;
        //then
        assertThat(result, is(100.10f));
    }


    @Test
    public void promotion_princing_process_test() {

        //given
        Float result = 0.0f;
        ProductOffer productA = new ProductOffer(0.0f, 10, "item4", 22f);
        productA.setPromotion(true);
        ProductOffer productB = new ProductOffer(0.0f, 6, "item2", 22f);
        productB.setPromotion(true);
        productListMap.put("item4", productA);
        productListMap.put("item2", productB);

        result = supermarket.promotionPricingProcess(productListMap, result);
        result = ((float)((int)(result * 100))) / 100;

        //then
        assertThat(result, is(161.00f));
    }

    @Test
    public void princing_process_get_one_free_test() {

        //given
        Float result = 0.0f;
        ProductOffer productA = new ProductOffer(0.0f, 4, "item5", 22f);
        productA.setFreePromotion(true);
        ProductOffer productB = new ProductOffer(0.0f, 7, "item6", 22f);
        productB.setFreePromotion(true);
        productListMap.put("item5", productA);
        productListMap.put("item6", productB);

        result = supermarket.processPricingGetOneFreeMap(productListMap, result);
        result = ((float)((int)(result * 100))) / 100;

        //then
        assertThat(result, is(198.00f));
    }


    @Test
    public void calculate_bill_test() {

        //given
        Float result = 0.0f;
        productCart.addProduct(new ProductOffer(null, 10, "item5", 22f));
        ProductOffer product = new ProductOffer(2.4f, 10, "item6", 10f);
        product.setUnit(Unit.POUND.getValue());
        productCart.addProduct(product);

        ProductOffer productB = new ProductOffer(null, 6, "item2", 22f);
        productB.setPromotion(true);
        productCart.addProduct(productB);
        ProductOffer productC = new ProductOffer(null, 6, "item7", 22f);
        productC.setFreePromotion(true);
        productCart.addProduct(productC);

        result = supermarket.calculateBill(productCart);

        //then
        assertThat(result, is(421.00f));
    }

}