package test.java.kata.ingeniance.sg.service;
import main.java.kata.ingeniance.sg.model.ProductOffer;
import main.java.kata.ingeniance.sg.service.ProductCart;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.*;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.*;

public class ProductCartTest {

    private ProductOffer productA, productB;
    private ProductCart productCart;
    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
        System.setOut(originalOut);
    }

    @Test
    public void review_cart_test() throws Exception {
        this.productCart = new ProductCart();
        productA = new ProductOffer(0.0f, 10, "item5", 22f);
        this.productCart.addProduct(productA);
        this.productCart.reviewCart();
        assertEquals("product: " + productA.getName() + " of Quantity: " + productA.getQuantity() + "\n", outContent.toString());
    }

    @Test
    public void add_product_cart_test() throws Exception {
        //given
        this.productCart = new ProductCart();
        productA = new ProductOffer(0.0f, 10, "item5", 22f);
        this.productCart.addProduct(productA);
        //then
        assertThat(this.productCart.findProductByName("item5").get(), is(productA));
    }

    @Test
    public void add_product_cart_Exception_test() throws Exception {
        try {
            this.productCart = new ProductCart();
            productA = new ProductOffer(0.0f, 10, "item5", 22f);
            this.productCart.addProduct(productA);
            this.productCart.addProduct(productA);
            fail("IllegalStateException not thrown");
        } catch (IllegalStateException expected) {
            assertEquals("The product with 'item5' name is already existed.", expected.getMessage());
        }
    }

    @Test
    public void remove_from_cart_test() throws Exception {
        //given
        this.productCart = new ProductCart();
        productA = new ProductOffer(0.0f, 10, "item5", 22f);
        productB = new ProductOffer(0.0f, 6, "item2", 22f);

        this.productCart.addProduct(productA);
        this.productCart.addProduct(productB);


        this.productCart.removeProduct(productA, 5);
        this.productCart.removeProduct(productB, 3);

        //then
        assertThat(this.productCart.findProductByName("item2").get().getQuantity(), is(productB.getQuantity()));
        assertThat(this.productCart.findProductByName("item5").get().getQuantity(), is(productA.getQuantity()));


        this.productCart.removeProduct(productA, 5);
        this.productCart.removeProduct(productB, 3);

        //then
        assertThat(this.productCart.findProductByName("item2").isEmpty(), is(true));
        assertThat(this.productCart.findProductByName("item5").isEmpty(), is(true));

    }

    @Test
    public void remove_from_cart_test_Exception_Name_Test() throws Exception {
        try {
            //given
            this.productCart = new ProductCart();
            productA = new ProductOffer(0.0f, 10, "item5", 22f);
            productB = new ProductOffer(0.0f, 10, "item12", 22f);
            this.productCart.addProduct(productA);
            this.productCart.removeProduct(productB, 12);

            fail("Exception not thrown");
        } catch (Exception expected) {
            assertEquals("No such product in your cart", expected.getMessage());
        }
    }

    @Test
    public void remove_from_cart_test_Exception_Quantity_Test() throws Exception {
        try {
            //given
            this.productCart = new ProductCart();
            productA = new ProductOffer(0.0f, 10, "item5", 22f);
            this.productCart.addProduct(productA);
            this.productCart.removeProduct(productA, 12);
            fail("Exception not thrown");
        } catch (Exception expected) {
            assertEquals("Quantity for the provided product is more than the quantity in the cart", expected.getMessage());
        }
    }
}