package main.java.kata.ingeniance.sg.utils;

import main.java.kata.ingeniance.sg.model.ProductOffer;

import java.util.HashMap;
import java.util.Map;

/**
 *  The Dictionary of products on promotion
 */
public class ProductDictionary {

    public static Map <String,ProductOffer > productGetOneFreeMap = new HashMap <String, ProductOffer> ();
    public static Map <String, ProductOffer> productOfferMap = new HashMap <String, ProductOffer> ();

    static {
        productOfferMap.put("item2", new ProductOffer(0.0f, 3, "item2", 1f));
        productOfferMap.put("item4", new ProductOffer(0.0f, 6, "item4", 6f));
        productGetOneFreeMap.put("item3", new ProductOffer(0.0f, 2, "item3", 0f));
        productGetOneFreeMap.put("item5", new ProductOffer(0.0f, 3, "item5", 0f));
        productGetOneFreeMap.put("item6", new ProductOffer(0.0f, 5, "item6", 0f));
        productGetOneFreeMap.put("item7", new ProductOffer(0.0f, 5, "item7", 0f));
    }

    public static Map<String, ProductOffer> getProductGetOneFreeMap() {
        return productGetOneFreeMap;
    }

    public static void addProductGetOneFreeMap(ProductOffer product) {
        productGetOneFreeMap.computeIfPresent(product.getName(), (name, existedProduct) -> {
            throw new IllegalStateException("The product with \'" + name + "\' name is already existed.");
        });

        productGetOneFreeMap.put(product.getName(), product);
    }

    public static Map<String, ProductOffer> getProductOfferMap() {
        return productOfferMap;
    }

    public static void addProductOfferMap(ProductOffer product) {
        productOfferMap.computeIfPresent(product.getName(), (name, existedProduct) -> {
            throw new IllegalStateException("The product with \'" + name + "\' name is already existed.");
        });
        productOfferMap.put(product.getName(), product);
    }
}