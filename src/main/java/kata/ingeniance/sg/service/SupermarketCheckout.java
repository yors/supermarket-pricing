package main.java.kata.ingeniance.sg.service;

import main.java.kata.ingeniance.sg.model.ProductOffer;
import main.java.kata.ingeniance.sg.pricing.DefaultPricing;
import main.java.kata.ingeniance.sg.pricing.PricingByWeight;
import main.java.kata.ingeniance.sg.pricing.PricingGetOneFree;
import main.java.kata.ingeniance.sg.pricing.PromotionPricing;

import java.util.Map;

public class SupermarketCheckout {

    /**
     *
     * @param productCart the product cart
     * @return the global price of all products purchased
     */
    public Float calculateBill(ProductCart productCart) {
        Map <String,ProductOffer> productListMap = productCart.getProductListMap();
        float productBill = 0f;

        productBill = processDefaultPricing(productListMap, productBill);
        productBill = promotionPricingProcess(productListMap, productBill);
        productBill = processPricingByWeight(productListMap, productBill);
        productBill = processPricingGetOneFreeMap(productListMap, productBill);
        productBill = ((float)((int)(productBill * 100))) / 100;
        return productBill;
    }

    /**
     * Calculate the overall price of products on promotion
     * @param productListMap the products
     * @param bill the bill
     * @return the price
     */
    public float promotionPricingProcess(Map <String,ProductOffer> productListMap, float bill) {
        return productListMap.entrySet().stream()
                .filter(item -> item.getValue().isPromotion())
                .map(item -> new PromotionPricing().calculatePrice(productListMap.get(item.getKey())))
                .reduce(bill, (f1, f2) -> f1 + f2);
    }

    /**
     * Calculate the overall price of products whitout promotion
     * @param productListMap the products
     * @param bill the bill
     * @return the price
     */
    public float processDefaultPricing(Map <String, ProductOffer> productListMap, float bill) {
        return productListMap.entrySet().stream()
                .filter(item -> (item.getValue().getGlobalWeight() == null && !item.getValue().isFreePromotion() && !item.getValue().isPromotion()))
                .map(entry -> new DefaultPricing().calculatePrice(productListMap.get(entry.getKey())))
                .reduce(bill, (f1, f2) -> f1 + f2);
    }

    /**
     * Calculate the overall price of products by weight
     * @param productListMap the products
     * @param bill the bill
     * @return the price
     */
    public float processPricingByWeight(Map <String, ProductOffer> productListMap, float bill) {
        return productListMap.entrySet().stream()
                .filter(item -> item.getValue().getGlobalWeight() != null)
                .map(item -> new PricingByWeight().calculatePrice(productListMap.get(item.getKey())))
                .reduce(bill, (f1, f2) -> f1 + f2);
    }

    /**
     * Calculate the overall price of products on promotion (get one free)
     * @param productListMap the products
     * @param bill the bill
     * @return the price
     */
    public float processPricingGetOneFreeMap(Map <String,ProductOffer> productListMap, float bill) {
        return productListMap.entrySet().stream()
                .filter(item -> item.getValue().isFreePromotion())
                .map(item -> new PricingGetOneFree().calculatePrice(productListMap.get(item.getKey())))
                .reduce(bill, (f1, f2) -> f1 + f2);
    }

}