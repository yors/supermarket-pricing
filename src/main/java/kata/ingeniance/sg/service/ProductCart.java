package main.java.kata.ingeniance.sg.service;

import main.java.kata.ingeniance.sg.model.Product;
import main.java.kata.ingeniance.sg.model.ProductOffer;

import java.util.*;
import java.util.stream.Collectors;

public class ProductCart {


    private Map<String, ProductOffer> productListMap;


    public ProductCart() {
        productListMap = new HashMap<>();
    }

    /**
     *
     * @param product add product to the existing cart
     */
    public void addProduct(ProductOffer product) {
        productListMap.computeIfPresent(product.getName(), (name, existedProduct) -> {
            throw new IllegalStateException("The product with \'" + name + "\' name is already existed.");
        });

        productListMap.put(product.getName(), product);
    }

    /**
     *
     * @param name  the product name
     * @return {@link Product}
     */
    public Optional <Product> findProductByName(String name) {
        return Optional.ofNullable(productListMap.get(name));
    }


    // Method to remove products along with their quantities from the existing cart
    public void removeProduct(ProductOffer product, int quantity) throws Exception {

        // If the product is not in the cart then throw exception
        if (!productListMap.containsKey(product.getName())) {
            throw new Exception("No such product in your cart");
        }

        if (quantity == productListMap.get(product.getName()).getQuantity()) {
            productListMap.remove(product.getName());
        } else if (productListMap.get(product.getName()).getQuantity() > quantity) {
            productListMap.get(product.getName()).setQuantity(productListMap.get(product.getName()).getQuantity() - quantity);
        } else {
            throw new Exception("Quantity for the provided product is more than the quantity in the cart");
        }
    }

    // Method to preview the content of the cart and all products in it
    public void reviewCart() throws Exception {
        if (productListMap.isEmpty()) {
            throw new Exception("Your cart is empty");
        }

        // Iterate through the cart and display the products  with their quantities
        productListMap.entrySet().stream().forEach(entry -> {
            ProductOffer product = entry.getValue();
            System.out.println("product: " + product.getName() + " of Quantity: " + product.getQuantity());
        });
    }

    // Method to empty the cart
    public void emptyCart() {
        productListMap.clear();
    }

    public Map < String, ProductOffer > getProductListMap() {
        return productListMap;
    }

    public void setProductListMap(Map <String,ProductOffer > productListMap) {
        this.productListMap = productListMap;
    }


    @Override
    public String toString() {

        return this.productListMap.entrySet()
                .stream()
                .map(entry -> " Name: " + entry.getKey() + " - " + " Price: " + entry.getValue().getPrice() + " - " + " Quantity: " + entry.getValue().getQuantity())
                .collect(Collectors.joining(", "));
    }
}