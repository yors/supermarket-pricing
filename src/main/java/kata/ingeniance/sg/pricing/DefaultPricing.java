package main.java.kata.ingeniance.sg.pricing;

import main.java.kata.ingeniance.sg.model.ProductOffer;

/**
 * Default pricing
 */
public class DefaultPricing extends Pricing {

    @Override
    public float calculatePrice(ProductOffer product) {
        return product.getQuantity() * product.getPrice();
    }
}