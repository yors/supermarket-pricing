package main.java.kata.ingeniance.sg.pricing;

import main.java.kata.ingeniance.sg.model.ProductOffer;
import main.java.kata.ingeniance.sg.model.Unit;


/**
 * Pricing by weight
 */
public class PricingByWeight extends Pricing {

    static public float OUNCES_TO_POUND = 0.0625f;

    @Override
    public float calculatePrice(ProductOffer product) {
        float total = 0.0f;
        if (product.getGlobalWeight() != null) {
            if (Unit.POUND.getValue() == product.getUnit()) {
                total += product.getPrice() * product.getGlobalWeight();
            } else if (Unit.OUNCES.getValue() == product.getUnit()) {
                total += product.getPrice() * product.getGlobalWeight() * OUNCES_TO_POUND;
            }
        }
        return total;
    }
}