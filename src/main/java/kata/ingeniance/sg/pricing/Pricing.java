package main.java.kata.ingeniance.sg.pricing;

import main.java.kata.ingeniance.sg.model.ProductOffer;

public abstract class  Pricing {

    /**
     *
     * @param product the product
     * @return the total price
     */
    public abstract float calculatePrice(ProductOffer product);
}
