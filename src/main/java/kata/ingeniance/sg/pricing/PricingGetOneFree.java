package main.java.kata.ingeniance.sg.pricing;

import main.java.kata.ingeniance.sg.model.ProductOffer;
import main.java.kata.ingeniance.sg.utils.ProductDictionary;

/**
 * Pricing Free: Buy x product(s) and get One free
 */
public class PricingGetOneFree extends Pricing {
    @Override
    public float calculatePrice(ProductOffer product) {

        float total = 0.0f;
        Integer quantity = product.getQuantity();
        if (ProductDictionary.productGetOneFreeMap.containsKey(product.getName())) {
            ProductOffer twoFree = ProductDictionary.productGetOneFreeMap.get(product.getName());
            if (quantity > twoFree.getQuantity()) {
                total += product.getPrice() * (quantity - 1);
            } else {
                total += product.getPrice() * quantity;
            }
        }
        return total;
    }
}