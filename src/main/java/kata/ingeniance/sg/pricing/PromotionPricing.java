package main.java.kata.ingeniance.sg.pricing;

import main.java.kata.ingeniance.sg.model.ProductOffer;
import main.java.kata.ingeniance.sg.utils.ProductDictionary;

/**
 * Promotion Princing Package: x products  for 1|2 or 3.. $
 */
public class PromotionPricing extends Pricing {

    @Override
    public float calculatePrice(ProductOffer product) {

        float total = 0.0f;
        Integer quantity = product.getQuantity();
        if (ProductDictionary.productOfferMap.containsKey(product.getName())) {
            ProductOffer thisOffer = ProductDictionary.productOfferMap.get(product.getName());
            if (thisOffer.getQuantity() < quantity) {
                total += thisOffer.getPrice() + product.getPrice() * (quantity - thisOffer.getQuantity());
            } else if (thisOffer.getQuantity() == quantity) {
                total += thisOffer.getPrice();
            } else {
                total += product.getPrice() * quantity;
            }
        }
        return total;
    }

}