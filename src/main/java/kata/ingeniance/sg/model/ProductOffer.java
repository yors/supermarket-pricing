package main.java.kata.ingeniance.sg.model;


/**
 *  The model of the product on promotion
 */
public class ProductOffer extends Product {

    private boolean promotion;
    private boolean freePromotion;

    public ProductOffer() {
        super();
    }

    public ProductOffer(Float globalWeight, Integer quantity, String name, float price) {
        super(globalWeight, quantity, name, price);
    }

    public ProductOffer(boolean promotion, boolean freePromotion) {
        this.promotion = promotion;
        this.freePromotion = freePromotion;
    }

    public boolean isPromotion() {
        return promotion;
    }

    public void setPromotion(boolean promotion) {
        this.promotion = promotion;
    }

    public boolean isFreePromotion() {
        return freePromotion;
    }

    public void setFreePromotion(boolean freePromotion) {
        this.freePromotion = freePromotion;
    }
}