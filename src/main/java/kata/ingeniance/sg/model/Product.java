package main.java.kata.ingeniance.sg.model;
import java.util.Objects;

/**
 *  The product model
 */
public class Product {

    private Integer quantity;

    // Global weight from the quantity of product
    private Float globalWeight;
    private String name;

    //1=pound,2=ounce
    private int unit;
    private float price;

    public Product() {
        this.globalWeight = 0.0f;
        this.quantity = 0;
        this.price = 0.0f;
        this.unit = 1;
    }

    public Product(Float globalWeight, Integer quantity, String name, float price) {
        this.globalWeight = globalWeight;
        this.quantity = quantity;
        this.name = name;
        this.price = price;
    }

    public int getUnit() {
        return unit;
    }

    public void setUnit(int unit) {
        this.unit = unit;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Float getGlobalWeight() {
        return globalWeight;
    }

    public void setGlobalWeight(Float globalWeight) {
        this.globalWeight = globalWeight;
    }



    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof Product)) {
            return false;
        }

        Product product = (Product) o;
        return Objects.equals(name, product.name) &&
                Objects.equals(price, product.price);
    }


    @Override
    public int hashCode() {
        return Objects.hash(name, price);
    }

}