package main.java.kata.ingeniance.sg.model;

/**
 *  The units
 */
public enum Unit {
    POUND(1),
    OUNCES(2);

    private int value;

    Unit(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }
}